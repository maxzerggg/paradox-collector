(()=>{
	"use strict"

	const fs 		= require( 'fs' );
	const path		= require( 'path' )
	const pemu 		= require( 'paraemu' );
	const levelkv 	= require( 'levelkv' );
	const mongo		= require( 'mongointerface' );
	const logger 	= require( 'eggglogger').CreateLogger({isDebug:true, writeFile:"record.txt", isNeedTimeStamp:true});

	const protocol = Object.freeze({
		REGISTER_NODE_REQ: "register-node-req",
		REGISTER_NODE_ACK: "register-node-ack",
		DELETE_NODE_REQ: "delete-node-req",
		DELETE_NODE_ACK: "delete-node-ack",

		I_AM_COLLECTOR: "IamCollector",
		I_AM_ANALYSIS: "IamAnalysis",
		ANALYSIS_NOTIFY_REQ: "analysis-notify-req",
		ANALYSIS_NOTIFY_ACK: "analysis-notify-ack",
		ANALYSIS_NOTIFY_START: "analysis-notify-start",
		ANALYSIS_SEND_NODE_START: "analysis-send-node-start",
		ANALYSIS_NODE: "analysis-node",
		ANALYSIS_SEND_NODE_END: "analysis-send-node-end",
		ANALYSIS_NOTIFY_END: "analysis-notify-end",
	});
	const process = Object.freeze({
		TASKS_READY: "tasks-ready",
		SNAPSHOT: "snapshot",
	});

	const {nodeSrc} = pemu.args;

	const MONGO_DB_NAME = nodeSrc ? nodeSrc : null;
	const LEVELKV_DB_PATH = nodeSrc ? `../07_Collector_Data/${Date.now()}` : `../07_Collector_Data/nodeDB[ 10000, 3000, 10, 0.002, 10 ]`;
	const THREAD_SIMULATE_NODE_NUM = 3;
	const BATCH_STRING_LENGTH = 1024;
	const MAX_THREAD = 4;
	const PROGREE_INTERVAL = 60000;


	let g_dbInst	= null;
	let g_colls		= {};
	let g_nodeDB	= {};//TODO: 以後可能會有node在送batch分析時 斷線/新增
	let g_analysis	= null;
	let g_batchId	= null;// 每次snapshot登記batchId, 整批傳完後清掉
	let g_progress  = {};

	try{
		// 額外工作: 把 mongoDB 資料 copy 到 levelkv
		pemu
		.on(process.TASKS_READY, async ()=>{
			logger.Log(`[collector]--TASKS_READY`);

			// 建立 thread
			// for(let i=0; i<MAX_THREAD; i++){
			// 	let thread = await pemu.job("./thread.js", {workerData:{parent:pemu.uniqueId, id:i}});
			// 	g_idleThread.push(thread.uniqueId);
			// 	logger.Log(`  [analysis][process]create thread_${i}`);
			// }

			if( MONGO_DB_NAME ){
				// 從mongoDB來源模擬建立各node自己的levelDB
				let time1 = Date.now();
				let dbConnectinoInst = await mongo.connect("localhost", 27017);
				if( !dbConnectinoInst ){
					throw new Error('db connect is fail...');
				}
				g_dbInst = await mongo.initDB( MONGO_DB_NAME );
				if( !g_dbInst ){
					throw new Error('init db "catch" is fail...');
				}
				g_colls.node 	= g_dbInst.collection(`sim_nodes`);
				g_colls.block 	= g_dbInst.collection(`sim_blocks`);
				g_colls.txn 	= g_dbInst.collection(`sim_txns`);


				let nodesNum = await g_colls.node.countDocuments({}, {hint: "id"});
				logger.Log(`[collector]nodesNum(${nodesNum})`);

				let nodesNumCopy = nodesNum;
				___PROGRESS_START("buildNode", nodesNumCopy, PROGREE_INTERVAL);
				while( nodesNumCopy-- > 0 ){
					___PROGRESS_ADD("buildNode");
					let node = null;
					await g_colls.node.findOneAndDelete({}, {projection: {_id:0, travel:0, block:0}})
						.then((r)=>{
							node = r.value;
						});

					await ___CREATE_DIR(LEVELKV_DB_PATH+`/${node.id}_state`);
					await ___CREATE_DIR(LEVELKV_DB_PATH+`/${node.id}_blocks`);
					await ___CREATE_DIR(LEVELKV_DB_PATH+`/${node.id}_txns`);

					// state
					let stateDB = await levelkv.initFromPath(LEVELKV_DB_PATH+`/${node.id}_state`);
					await stateDB.put( "id", node.id );
					await stateDB.put( "peers", node.peers );
					await stateDB.put( "latest", node.latest );
					await stateDB.close();
					// logger.Log(`[collector]state ${node.id}`);

					// blocks
					let blockHash = node.latest;
					let blockDB = await levelkv.initFromPath(LEVELKV_DB_PATH+`/${node.id}_blocks`);
					while( blockHash !== "0000000000000000000000000000000000000000000000000000" ){
						await g_colls.block.findOne({hash:blockHash}, {projection: {_id:0, num:0}})
							.then(async (r)=>{
								if( r ){
									await blockDB.put( r.hash, r );
									blockHash = r.parent;
								}else{
									throw new Error(`missing block: ${blockHash}`);
								}
							});
					}
					await blockDB.close();
					// logger.Log(`[collector]blocks ${node.id}`);

					// txns
					let txnDB = await levelkv.initFromPath(LEVELKV_DB_PATH+`/${node.id}_txns`);
					for(let txnHash of node.txns){
						await g_colls.txn.findOne({hash:txnHash}, {projection: {_id:0}})
							.then(async (r)=>{
								if( r ){
									await txnDB.put( txnHash, r );
								}else{
									throw new Error(`missing txn: ${txnHash}`);
								}
							});
					}
					await txnDB.close();
					// logger.Log(`[collector]txns ${node.id}`);



					g_nodeDB[node.id] = {
						state: LEVELKV_DB_PATH+`/${node.id}_state`,
						blocks: LEVELKV_DB_PATH+`/${node.id}_blocks`,
						txns: LEVELKV_DB_PATH+`/${node.id}_txns`
					};
				}
				___PROGRESS_END("buildNode");
				logger.Log(`[collector]build node levelDB completely, ${Date.now()-time1}(ms)`);
			}else{
				let time1 = Date.now();
				const dirs = p => fs.readdirSync(p).filter(f => fs.statSync(path.join(p, f)).isDirectory())
				let dirNameArray = dirs(LEVELKV_DB_PATH);
				___PROGRESS_START("importNode", dirNameArray.length, PROGREE_INTERVAL);
				for(let dirName of dirNameArray){
					___PROGRESS_ADD("importNode");
					let temp = dirName.split("_");
					g_nodeDB[temp[0]] = !g_nodeDB[temp[0]] ? {} : g_nodeDB[temp[0]];
					g_nodeDB[temp[0]][temp[1]] = LEVELKV_DB_PATH+"/"+dirName;
				}
				___PROGRESS_END("importNode");
				logger.Log(`[collector]import node levelDB completely, ${Date.now()-time1}(ms)`);
			}
			// logger.Info(g_nodeDB);
			// return;

			// 開thread模擬node
			// let threadsNum = Math.ceil(nodesNum/THREAD_SIMULATE_NODE_NUM);
			// logger.Log(`[collector]--build ${threadsNum} threads`);
			// let nodeIds = Object.keys(g_nodeDB);
			// for(let i=0; i<threadsNum; i++){
			// 	let data = nodeIds.slice(i*THREAD_SIMULATE_NODE_NUM, (i+1)*THREAD_SIMULATE_NODE_NUM);
			// 	// logger.Log(`[collector]data ${data}`);
			// 	pemu.job("./nanny.js", { workerData: { nodeIds: data, id:i, parent:pemu.uniqueId} });
			// }
			// g_nodeDB = {};

			pemu.emit(protocol.I_AM_COLLECTOR, pemu.uniqueId);

			setTimeout(()=>{
				pemu.send(pemu.uniqueId, process.SNAPSHOT);
			}, 5000);
		})
		.on(protocol.I_AM_ANALYSIS, (e, analysisId)=>{
			logger.Log(`[collector]--I_AM_ANALYSIS  ${analysisId}`);
			g_analysis = analysisId;
		})
		.on(protocol.REGISTER_NODE_REQ, (e, nodeId, nodeDB)=>{
			if( !g_nodeDB.hasOwnProperty(nodeId) ){
				g_nodeDB[nodeId] = nodeDB;
				pemu.send(e.target, protocol.REGISTER_NODE_ACK, true);
			}
		})
		.on(protocol.DELETE_NODE_REQ, (e, nodeId)=>{
			if( g_nodeDB.hasOwnProperty(nodeId) ){
				delete g_nodeDB[nodeId];
				pemu.send(e.target, protocol.DELETE_NODE_ACK, true);
			}
		})
		.on(process.SNAPSHOT, async (e)=>{
			g_batchId = Date.now();
			logger.Info(`[collector]--SNAPSHOT(${g_batchId})`);

			//TODO: 呼叫levelKV鎖住功能 ...waiting for JCloud
			pemu.send(g_analysis, protocol.ANALYSIS_NOTIFY_REQ, {batchId: g_batchId});
		})
		.on(protocol.ANALYSIS_NOTIFY_ACK, async (e, {handler})=>{
			logger.Log(`[collector]--ANALYSIS_NOTIFY_ACK, handler(${handler})`);
			pemu.send(g_analysis, protocol.ANALYSIS_NOTIFY_START, {batchId: g_batchId});
			___PROGRESS_START(g_batchId, Object.keys(g_nodeDB).length, PROGREE_INTERVAL);

			for(let nodeId in g_nodeDB){
				___PROGRESS_ADD(g_batchId);
				pemu.send(handler, protocol.ANALYSIS_SEND_NODE_START, {batchId:g_batchId, nodeId});

				let node = {blocks:{}, transactions:{}};
				let levelDBCursor = null;
				let levelDBResult = null;

				// state
				let stateDB = await levelkv.initFromPath(g_nodeDB[nodeId].state);
				let work = ["id", "peers", "latest"];
				for(let w of work){
					levelDBCursor = await stateDB.get( w );
					levelDBResult = await levelDBCursor.toArray();
					node[w] = levelDBResult[0];
				}
				await stateDB.close();

				// blocks
				let blockDB = await levelkv.initFromPath(g_nodeDB[nodeId].blocks);
				let blockHash = node.latest;
				while( blockHash != "0000000000000000000000000000000000000000000000000000" ){
					levelDBCursor = await blockDB.get( blockHash );
					levelDBResult = await levelDBCursor.toArray();
					node.blocks[blockHash] = levelDBResult[0];

					blockHash = levelDBResult[0].parent;
				}
				await blockDB.close();

				// txns
				let txnDB = await levelkv.initFromPath(g_nodeDB[nodeId].txns);
				levelDBCursor = await txnDB.get();
				for await (let txn of levelDBCursor){
					node.transactions[txn.hash] = txn;
				}
				await txnDB.close();
				// logger.Log(node);


				let nodeStr = JSON.stringify(node);
				let nodeStrIter = 0;
				// let dataIndex = 0;

				while( nodeStrIter < nodeStr.length ){
					let sendData = nodeStr.slice(nodeStrIter, nodeStrIter+BATCH_STRING_LENGTH);
					pemu.send(handler, protocol.ANALYSIS_NODE, {batchId:g_batchId, nodeId, /*index:dataIndex++,*/ data:sendData});
					nodeStrIter += BATCH_STRING_LENGTH;
				}

				pemu.send(handler, protocol.ANALYSIS_SEND_NODE_END, {batchId:g_batchId, nodeId});
			}

			___PROGRESS_END(g_batchId);
			pemu.send(handler, protocol.ANALYSIS_NOTIFY_END, {batchId: g_batchId});
			g_batchId = null;
		})
	}catch(e){
		logger.Error(e);
		throw(e);
	}

	function ___CREATE_DIR (path, level=0){
		return new Promise((resolve)=>{
			let temp = path.split('/');
			if( level >= temp.length ){
				resolve();
				return;
			}

			let dir = "";
			for(let i=0; i<=level; i++){
				let mark = i===0 ? "" : "/";
				dir = dir+mark+temp[i];
			}
			if( !fs.existsSync(dir) ){
				fs.mkdirSync(dir);
			}
			___CREATE_DIR(path, level+1);
			resolve();
		});
	}
	function ___PROGRESS_START(key, total, time=600000){
		g_progress[key] = {total, count:0, timer:null};
		logger.Info(`[collector]${key} ${g_progress[key].count}/${g_progress[key].total} (${Math.round(g_progress[key].count*100000/g_progress[key].total)/1000}%)`);
		let t = setInterval(()=>{
			logger.Info(`[collector]${key} ${g_progress[key].count}/${g_progress[key].total} (${Math.round(g_progress[key].count*100000/g_progress[key].total)/1000}%)`);
		}, time)// 10 min
		g_progress[key].timer = t;
	}
	function ___PROGRESS_ADD(key, add=1){
		g_progress[key].count += add;
	}
	function ___PROGRESS_END(key){
		logger.Info(`[collector]${key} ${g_progress[key].count}/${g_progress[key].total} (${Math.round(g_progress[key].count*100000/g_progress[key].total)/1000}%)`);
		clearInterval(g_progress[key].timer);
		delete g_progress[key];
	}
})();